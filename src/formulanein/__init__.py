__version__ = "1.4.0"

from collections import namedtuple
from dataclasses import dataclass
from typing import Dict, List
from pathlib import Path
import argparse
import datetime
import json
import jinja2
import requests
import pprint
import sys

POINTS = [25, 18, 15, 12, 10, 8, 6, 4, 2, 1]
CACHE_FILE_TEMPLATE = "./cache/formulanein_{}.json"

DRIVER_ADJUSTMENTS = []
CONSTRUCTOR_ADJUSTMENTS = [
    # Racing Point brake ducts 2020
    lambda standing: -15
    if standing.constructorId == "racing_point" and standing.season == 2020
    else 0
]


@dataclass
class Standing:
    name: str
    constructorId: str
    constructor: str
    season: int
    points: int = 0
    wins: int = 0
    podiums: int = 0


def collect_season(season: int, reload_cache: bool = False) -> List:
    if reload_cache:
        # Force update from ergast
        return update_cache_from_ergast(season)
    else:
        # Try to use local cache, update from ergast if required
        try:
            return load_season_from_cache(season)
        except:
            return update_cache_from_ergast(season)


def update_cache_from_ergast(season: int) -> List:
    # Attempt to query ergast for race results, race-by-race
    races = []
    while len(races) < 30:  # Don't enumerate forever, just in case
        url = "https://ergast.com/api/f1/{}/{}/results.json".format(
            season, len(races) + 1
        )
        response = requests.get(url, params={"limit": 50})
        if response.status_code != 200:
            raise RuntimeError(
                "Unable to query ergast, error code {}".format(response.status_code)
            )
        result = response.json()
        if not result["MRData"]["RaceTable"]["Races"]:
            break
        races.append(result["MRData"]["RaceTable"]["Races"][0])
    # Save races to local cache
    cache_filename = CACHE_FILE_TEMPLATE.format(season)
    with open(cache_filename, "w") as cache_fh:
        json.dump(races, cache_fh)
    return races


def load_season_from_cache(season: int) -> List:
    # Load season race data from the local cache
    cache_filename = CACHE_FILE_TEMPLATE.format(season)
    races = []
    try:
        with open(cache_filename, "r") as cache_fh:
            races = json.load(cache_fh)
    except:
        # Cache not found, bail out
        raise RuntimeError("Unable to load races from cache")
    return races


def simulate_season(
    season: int,
    ignore_drivers: list = [],
    ignore_constructors: list = [],
    reload_cache: bool = False,
) -> List:
    races = collect_season(season, reload_cache=reload_cache)
    for race in races:
        race = simulate_race(
            race, ignore_drivers=ignore_drivers, ignore_constructors=ignore_constructors
        )
    return races


def simulate_race(
    race: Dict, ignore_drivers: list = [], ignore_constructors: list = [],
) -> Dict:
    # Recreate the results as though provided drivers/constructors did not exist
    simulated_results = []
    position = 1
    overall_fastest_lap_position = 0
    overall_fastest_lap_rank = 100
    race_laps = float(
        race["Results"][0]["laps"]
    )  # Assume first place actually finished, haha
    for result in race["Results"]:
        if (
            not result["Constructor"]["constructorId"] in ignore_constructors
            and not result["Driver"]["driverId"] in ignore_drivers
        ):
            # Drivier can be classified if at least 90% of the race was completed
            driver_laps = float(result["laps"])
            classified = True if driver_laps / race_laps > 0.9 else False

            # Points are only awarded if driver classified
            result["points"] = (
                POINTS[position - 1] if position < len(POINTS) + 1 and classified else 0
            )
            # Update position
            result["position"] = position
            result["positionText"] = position
            if position == 1:
                # Count wins
                result["win"] = 1
            elif position <= 3:
                # Count podiums
                result["podium"] = 1
            # Check for fastest lap
            try:
                driver_fastest_lap_rank = int(result["FastestLap"]["rank"])
                if driver_fastest_lap_rank < overall_fastest_lap_rank:
                    overall_fastest_lap_rank = driver_fastest_lap_rank
                    overall_fastest_lap_position = position
            except KeyError:
                # Driver did not complete a lap :(
                pass
            simulated_results.append(result)
            position += 1
    # Award fastest lap point if in top ten
    if simulated_results[overall_fastest_lap_position - 1]["points"]:
        simulated_results[overall_fastest_lap_position - 1]["points"] += 1
    race["Results"] = simulated_results
    return race


def aggregate_standings(races: List) -> Dict:
    # Total points per driver for a given list of races
    # There is definitely a simpler way to do this but my brain is gone
    driver_standings = {}
    constructor_standings = {}
    season = int(races[0]["season"])
    for race in races:
        for result in race["Results"]:
            # Add driver standing
            driverId = result["Driver"]["driverId"]
            if not driverId in driver_standings:
                driver_standings[driverId] = Standing(
                    name=result["Driver"]["familyName"],
                    constructor=result["Constructor"]["name"],
                    constructorId=result["Constructor"]["constructorId"],
                    season=season,
                )
            driver_standings[driverId].points += result["points"]
            driver_standings[driverId].wins += result.get("win", 0)
            driver_standings[driverId].podiums += result.get("podium", 0)

    # Apply driver points modifications
    for standing in driver_standings.values():
        for adjustment in DRIVER_ADJUSTMENTS:
            standing.points += adjustment(standing)

    # Generate constructor standings from driver standings
    for driver_standing in driver_standings.values():
        # Add constructor standing
        constructor = driver_standing.constructor
        if not constructor in constructor_standings:
            constructor_standings[constructor] = Standing(
                name=constructor,
                constructor=constructor,
                constructorId=driver_standing.constructorId,
                season=season,
            )
        constructor_standings[constructor].points += driver_standing.points
        constructor_standings[constructor].wins += driver_standing.wins
        constructor_standings[constructor].podiums += driver_standing.podiums

    # Apply constructor points modifications
    for standing in constructor_standings.values():
        for adjustment in CONSTRUCTOR_ADJUSTMENTS:
            standing.points += adjustment(standing)

    # Sort standings by point totals
    sorted_driver_standings = sorted(
        driver_standings.values(), key=lambda x: x.points, reverse=True
    )
    sorted_constructor_standings = sorted(
        constructor_standings.values(), key=lambda x: x.points, reverse=True
    )
    return (sorted_driver_standings, sorted_constructor_standings)


def print_season(races: List) -> None:
    season = races[0]["season"]
    (driver_standings, constructor_standings) = aggregate_standings(races)
    print_driver_standings(driver_standings, season)
    print()
    print_constructor_standings(constructor_standings, season)
    print()
    for race in races:
        print_race(race)
        print()


def print_driver_standings(driver_standings: Dict, season: int) -> None:
    title = "{} driver standings".format(season)
    print(title)
    print("=" * len(title))
    template = "{:<20} {:<20} {:<8} {:<7} {:<8}"
    print(template.format("Driver", "Team", "Points", "Wins", "Podiums"))
    for standing in driver_standings:
        print(
            template.format(
                standing.name,
                standing.constructor,
                standing.points,
                standing.wins if standing.wins > 0 else "",
                standing.podiums if standing.podiums > 0 else "",
            )
        )


def print_constructor_standings(constructor_standings: Dict, season: int) -> None:
    title = "{} constructor standings".format(season)
    print(title)
    print("=" * len(title))
    template = "{:<20} {:<8}"
    print(template.format("Team", "Points"))
    for standing in constructor_standings:
        print(template.format(standing.constructor, standing.points))


def print_race(race: Dict) -> None:
    title = "{} {}".format(race["season"], race["raceName"])
    print(title)
    print("-" * len(title))
    template = "{:<4} {:<20} {:<20} {:<3}"
    print(template.format("Pos.", "Driver", "Team", "Points"))
    for result in race["Results"]:
        print(
            template.format(
                result["position"],
                result["Driver"]["familyName"],
                result["Constructor"]["name"],
                result["points"],
            )
        )


def generate_html(season: int, races: List) -> None:
    template_env = jinja2.Environment(
        loader=jinja2.PackageLoader("formulanein", "templates")
    )
    if races:
        template = template_env.get_template("season.html")
        (driver_standings, constructor_standings) = aggregate_standings(races)
        output = template.render(
            season=season,
            races=races,
            driver_standings=driver_standings,
            constructor_standings=constructor_standings,
        )
    else:
        template = template_env.get_template("notstarted.html")
        output = template.render(
            season=season
        )
    output_dir = "public/{}/".format(season)
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    with open(output_dir + "index.html", "w") as html_fh:
        html_fh.write(output)


def main():
    parser = argparse.ArgumentParser(
        description="Simulate F1 results without particular drivers or constructors"
    )
    parser.add_argument(
        "--seasons", type=int, nargs="*", help="Seasons to simulate", default=[]
    )
    parser.add_argument(
        "--seasons-since", type=int, help="Seasons to simulate, given year to present"
    )
    parser.add_argument(
        "--ignore-constructors",
        type=str,
        nargs="*",
        default=[],
        help="Constructor to ignore. More than one may be specified.",
    )
    parser.add_argument(
        "--ignore-drivers",
        type=str,
        nargs="*",
        default=[],
        help="Constructor to ignore. More than one may be specified.",
    )
    parser.add_argument("--html", action="store_true", help="Generate HTML output")

    args = parser.parse_args()

    seasons = args.seasons
    if not seasons and args.seasons_since:
        if args.seasons_since < 2014:
            raise RuntimeError(
                "Refusing to run with unsafe starting season value ({})".format(
                    args.seasons_since
                )
            )
        seasons = range(args.seasons_since, datetime.datetime.now().year + 1)

    for season in seasons:
        races = simulate_season(
            season,
            ignore_constructors=args.ignore_constructors,
            ignore_drivers=args.ignore_drivers,
        )
        if args.html:
            generate_html(season, races)
        else:
            print_season(races)


if __name__ == "__main__":
    main()
