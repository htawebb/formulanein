# formulanein

This project simulates Formula 1 standings as if Mercedes did not participate. It can be configured to ignore other constructors and drivers as desired.

https://kgmonteith.gitlab.io/formulanein/
